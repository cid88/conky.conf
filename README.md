# conky.conf

My conky configuration.

[![screenshot](https://gitlab.com/cid88/conky.conf/raw/master/screenshot_thumb.png)](https://gitlab.com/cid88/conky.conf/raw/master/screenshot.png)

Conky is a free, light-weight system monitor for X, that displays any kind of information on your desktop.
https://github.com/brndnmtthws/conky
